<?
$host = "localhost";
$user = "root";
$pass = "";
$dbname = "shop";
$mysqli = new mysqli($host, $user, $pass, $dbname);
$mysqli->set_charset("utf8");
$query = "SELECT * from products";
$table = $mysqli->query($query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Тестовое задание</title>
    <link rel="stylesheet" type="text/css" href="main.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.modal.css">
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="js/jquery.modal.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" >
</head>
<body>
	<div class="container">
        <button class="add_record" type="submit" >Добавить запись</button>
		<table class="table table-hover table-bordered" id="orders">
        <thead>
			<tr class="active">
				<th>ID</th>
				<th>Название</th>
				<th>Категория</th>
                <th>Описание</th>
                <th>Цена</th>
			</tr>
        </thead>
        <tbody>
			<?foreach ($table as $rows) {?>
				<tr class="info edit">
					<td class="id"><?=$rows['id']?></td>
					<td class="title"><?=$rows['title']?></td>
					<td class="category"><?=$rows['category']?></td>
                    <td class="description"><?=$rows['description']?></td>
                    <td class="price"><?=$rows['price']?></td>
				</tr>
			<?}?>
		</tbody>
		</table>
	</div>
    <div id="edit-record" style="display:none;">
        <form class="contact_form"  action="edit.php" method="post" >
            <ul>
                <li>
                    <h2>Редактировать запись</h2>
                </li>
                <li>
                    <label for="label_id">ID</label>
                    <input id="modal_id" name="modal_id" type="text" onkeydown="if(event.keyCode==13){return false;}">
                </li>
                <li>
                    <label for="label_title">Название</label>
                    <input id="modal_title" name="modal_title" type="text" onkeydown="if(event.keyCode==13){return false;}">
                </li>
                <li>
                    <label for="label_category">Категория</label>
                    <input id="modal_category" name="modal_category" type="text" onkeydown="if(event.keyCode==13){return false;}">
                </li>
                <li>
                    <label for="label_description">Описание</label>
                    <textarea id="modal_descriptions" name="modal_descriptions" cols="40" onkeydown="if(event.keyCode==13){return false;}"></textarea>
                </li>
                <li>
                    <label for="label_price">Цена</label>
                    <input id="modal_price" name="modal_price" type="text" onkeydown="if(event.keyCode==13){return false;}">
                </li>
                <li>
                    <button class="submit" type="submit" onclick="closeWindow()">Сохранить запись</button>
                </li>
            </ul>
        </form>
    </div>
	<script>
        $(document).ready(function(){
            $(document).on('click', 'tr.edit', function(){
                var id = $(this).children(".id").text();
                var title = $(this).children(".title").text();
                var category = $(this).children(".category").text();
                var description = $(this).children(".description").text();
                var price = $(this).children(".price").text();
                $.ajax({
                    type:"POST",
                    url:"ajax.php",
                    data:
                        {
                            id: id,
                            title: title,
                            category: category,
                            description: description,
                            price: price
                        },
                    success:function(result){
                        $("#modal_id").val(result.id);
                        $("#modal_title").val(result.title);
                        $("#modal_category").val(result.category);
                        $("#modal_descriptions").val(result.description);
                        $("#modal_price").val(result.price);
                    }
                });
                $('#edit-record').modal({
                    fadeDuration: 250,
                    fadeDelay: 0.80
                });
		    });
            $(function(){
                $('.contact_form').submit(function(e){
                    e.preventDefault();
                    $.modal.close();
                    var m_method=$(this).attr('method');
                    var m_action=$(this).attr('action');
                    var m_data=$(this).serialize();
                    $.ajax({
                        type: m_method,
                        url: m_action,
                        data: m_data,
                        success: function(result){
                            //$("orders tr:eq(result.id) td#id)=result.id;
                            alert($("orders tr:eq(result.id) td#id").html());
                            //alert($("tr.edit").rows[result.id]);
                           // $("tr.edit.title").val(result.title);
                            //$("tr.edit.category").val(result.category);
                            //$("tr.edit.description").val(result.description);
                           // $("tr.edit.price").val(result.price);
                        }
                    });
                });
            })
        });
    </script>
</body>
</html>